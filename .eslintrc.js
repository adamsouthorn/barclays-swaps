module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/recommended',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' && !process.env.ATOM_HOME ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' && !process.env.ATOM_HOME ? 'error' : 'off',
    'no-multiple-empty-lines': [
      'error', {
        'max': 2,
        'maxEOF': 1
      }
    ],
    'space-before-function-paren': [
      'error',
      'never'
    ],
    'semi': [
      'error',
      'never'
    ],
    'quotes': [
      'error',
      'single'
    ],
    'no-trailing-spaces': [
      'error', {
        'skipBlankLines': true
      }
    ],
    'vue/max-attributes-per-line': [2, {
      'singleline': 2
    }],
    'vue/component-name-in-template-casing': [
      'error',
      'kebab-case'
    ],
    'object-shorthand': [
      'error',
      'always'
    ],
    'vue/no-v-html': 'off',
    'vue/singleline-html-element-content-newline': 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
