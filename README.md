## Getting started

Ensure the latest Vue CLI (3.x) is installed:
```
npm install -g @vue/cli
```

Create a new project locally using the Vue CLI:
```
vue create <client-project-name>
```

Select the following prompt choices:

- Manually select features
- Babel, Router, Vuex, Linter / Formatter (use space bar to select)
- Type 'n' for Use history mode for router
- ESLint + Standard config
- Lint on save
- In dedicated config files
- Type 'y' for Save this as a preset... (so you won't have to go through these prompts again)

Now run the Cadence CLI plugin:
```
cd <client-project-name>
npm i -D git+file://uk-2k12fp-bri01/Git$/cadence/vue-cli-plugin-cadence.git
vue invoke cadence
```

Then, select these prompt choices:

- Type in the project ID (in kebab-case) or press Enter to pick the default
- Type in the project title (without the client name)
- Input the staging directory name (usually the client abbreviation in lowercase)
- Select the SCORM version (usually 1.2)
- Pick the components needed for the project (use space bar to select)

The project files and dependencies will now be set up.

To scaffold the example app, run:
```
npm run gen
```
Lastly, launch the dev server:
```
npm run serve
```
