module.exports = {
  plugins: {
    'postcss-import': {},
    'postcss-url': {},
    'postcss-flexbugs-fixes': {},
    'postcss-preset-env': {
      stage: 0,
      features: {
        'custom-properties': {
          preserve: false
        }
      },
      autoprefixer: {
        grid: 'no-autoplace'
      }
    },
    'postcss-calc': {}
  }
}
