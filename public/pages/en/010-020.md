{header}
==============================
## An overview

{body}
==============================
The regulatory environment governing the trading of financial derivatives is complex, and it is vital that you understand your role and responsibilities when executing trades on behalf of our clients. By the end of this course you will be able to:

- Explain how regulation impacts the trading of financial derivatives and swaps
- Identify which systems are available and where to turn to for support
- Demonstrate your understanding of your roles and responsibilities with regards to trading in financial derivatives and swaps
- Demonstrate a clear understanding of regulatory requirements and their relevance when using the Client Checklist Tool (CCLT)
- Understand the consequences of breaching regulation for both Barclays and yourself


{next}
==============================
Continue


{back}
==============================
Back