{header}
==============================
## Introduction

{body}
==============================
The Client Checklist Tool (CCLT) enables you to check your clients’ eligibility to trade according to certain variables of the trade and identify the classification of your counterparty. However, it is up to you to adhere to the relevant regulation throughout the trade lifecycle and when executing each transaction. There may be additional factors that you need to consider in adhering to the relevant regulation throughout the trade lifecycle and when executing each transaction.

For clarity on what and who is in scope of the regulations, please refer to the Client Checklist Tool by typing ‘CCLT’ into your browser (Chrome only).


{next}
==============================
Continue


{back}
==============================
Back