{header}
==============================
## Hannah in London


{body}
==============================
Hannah is a salesperson on the FX Sales desk in London. Her client will trade a block deal with her and then allocate to sub funds later in the day, which he said he will provide full details on. 

Does Hannah need to refer to the CCLT to check the status of the fund manager?

**Select the correct option.**


{q1}
==============================
- Yes
- No


{q1submit}
==============================
Answer


{right}
==============================
That’s right.


{wrong}
==============================
That’s not right.


{answer}
==============================
Hannah needs to refer to CCLT, for fund managers as many obligations apply to the client that we execute the trade with, especially if she does not know her clients current status with regards to the block trade and although rare, a CCLT status can change.

Where possible, Hannah should ask her client for the expected allocation of the trade in order to mitigate the risk that the allocation includes a counterparty that is red in the CCLT. Sometimes the allocation isn’t known and discussing this risk with her client will be important.


{next}
==============================
Continue


{back}
==============================
Back