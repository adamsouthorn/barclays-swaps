{header}
==============================
## Summary


{body}
==============================
You’ve now reached the end of this learning and know what it takes to ensure compliance when trading swaps and derivatives on behalf of our clients.

Here are the key takeaway points:

- Onboarding and pre-trade checks must be completed before your client is eligible to trade with us.
- Disclosures are mandated by ECBS and impact the way we interact with clients.
- If you believe the eligibility status of your client to be incorrect, then raise this to Legal and/or Compliance prior to trading.
- Trades must be executed in a timely manner according to the relevant regulation.
- We have a duty to report trading activity to the relevant bodies within regional specific requirements.
- Our trade reporting accuracy and timeliness depends on you booking your trades as soon as possible as many regulations now have real time reporting requirements. 

Now it’s time to show your understanding by taking the assessment. 


{next}
==============================
Continue


{back}
==============================
Back