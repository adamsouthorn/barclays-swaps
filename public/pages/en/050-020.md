{header}
==============================
## Question


{qz1body}
==============================
The CCLT is showing red status for a client. However, the client has confirmed that Barclays has received all the required documentation. Can we execute the trade?

**Select the correct option.**


{qz1}
==============================
- Yes. The CCLT must be wrong, as the client has informed you we are OK to trade. 
- No. There may be other factors that affect the client’s eligibility to trade. Check with Client Regulatory team or your local Compliance officer before you proceed with the trade.


{qz2body}
==============================
When do you need to check if your counterparty is operationally ready to clear their transaction?

**Select the correct option.**


{qz2}
==============================
- You must check CCLT before trading to determine whether a client is ready to trade.
- This can be checked post-trade on CCLT as this is a post-trade activity.


{qz3body}
==============================
You check CCLT for a client that wants to trade. Their status is red and indicates that we do not have an LEI for the client. What action do you need to take?

**Select the correct option.**


{qz3}
==============================
- Inform the client that their LEI is missing and is required in order for them to transact with Barclays Bank Plc.
- Execute the trade and ignore the CCLT status, as an LEI is not required.


{qz4body}
==============================
Nitin’s client has asked for the Pre-Trade Mid (PTM) of their FX forward transaction they are about to trade, as the client is subject to External Business Conducts Standards (EBCS). 

Which of the below statements is correct?

**Select the correct option.**


{qz4}
==============================
- The PTM is required to be a clean price, stripped of profit, credit charges, hedging costs, funding charges, and any other costs, or adjustments. It can be delivered in the same format as the price quote.
- The PTM is the arithmetic mean of the bid and offer price provided to the client.
- The PTM is not required as FX products are out of scope of EBCS.


{qz5body}
==============================
ESMA has made the use of an LEI mandatory to identify our clients on mandatory transactions reports that Barclays submits to regulators. Do all clients need an LEI to trade?

**Select the correct option.**


{qz5}
==============================
- All clients will need to have an LEI to trade with Barclays Bank Plc, Barclays Capital Securities Limited or Barclays Bank Ireland.
- All clients globally will need to have an LEI to trade regardless of the Barclays legal entity they trade with.


{qz6body}
==============================
A Client wants to trade a FX block trade. Which of the following apply?

**Select the correct option.**


{qz6}
==============================
- You must check CCLT for the client at the time of the trade to ensure the client’s status is green (i.e. OK to trade).
- Principal trading with a fund manager can only occur if the manager is overall green.
- All of the above.


{qz7body}
==============================
What defines a ‘US Person’?

**Select all of the correct options.**


{qz7}
==============================
- Their place of Incorporation is in the US.
- They have a Principal Place of Business in the US.
- The majority of their funds are owned by US Persons.
- Entities, not including fund entities, that meet other criteria.


{qz8body}
==============================
Trades are required to be reported as close to real time as technically possible. Which of the reporting restrictions apply?

**Select all of the correct options.**


{qz8}
==============================
- Equities should be reported no later than one minute after execution of the trade.
- Non-Equities should be reported no later than 15 minutes after execution of the trade.
- All products require reporting no later than 30 minutes after execution of the trade. 


{qz9body}
==============================
Your client is based in Frankfurt and guaranteed by an American company that is confirmed as a US Person in the CCLT.

Do you have to provide External Business Conduct Standards (EBCS) to them if you are trading a Swap?

**Select the correct option.**


{qz9}
==============================
- No, as your client is guaranteed by a US Person to trade swaps in scope of the Dodd-Frank Act they are not subject to EBCS.
- Yes. The link to a US person within their legal entity structure brings them into scope of EBCS. Unless they are a swap dealer, in which EBCS does not apply.


{qz10body}
==============================
What is a MiFID MTF?

**Select the correct option.**


{qz10}
==============================
- A European regulatory term for the traditional stock exchanges and listed derivatives exchanges. 
- A European regulatory term for a multilateral system operated by an investment firm or market operator, which brings together multiple third parties buying and selling interests in a non discretionary manner.


{qz11body}
==============================
You believe that your client’s checklist status is wrong and you want to trade. What should you do?

**Select the correct option.**


{qz11}
==============================
- Ignore it – someone will notice the problem and fix it.
- Contact the client regulatory hotline to have the issue investigated and corrected if incorrect prior to trading.
- Trade with your client as normal as the monitoring process will identify the exception and correct it later on.


{qz12body}
==============================
Under EMIR regulations for clearing, there are no time limits for them to be submitted after execution.
Is this correct?

**Select the correct option.**


{qz12}
==============================
- No. Eligible trades have to be submitted within 30 minutes of execution.
- No. Eligible trades have to be submitted within 15 minutes of execution.
- Yes. There are no time limits for submitting trades to clearing under EMIR regulations.


{qz13body}
==============================
Amanda is a Rates salesperson and her client wants Barclays to start clearing their transactions.

Which of the following can Amanda do?

**Select all of the correct options.**


{qz13}
==============================
- She can discuss the firm’s general services.
- She can suggest that the firm will offer clearing services to the client. 
- She can discuss or negotiate particular terms with the client before the Clearing Unit has made a clear decision to offer clearing services.


{qz14body}
==============================
Are cleared swaps and derivatives also subject to daily margining?

**Select the correct option.**


{qz14}
==============================
- Yes 
- No


{qz15body}
==============================
Your client is a US Person and asks you to provide details of your recent transactions static METs. Where would you find those details?

**Select the correct option.**

{qz15}
==============================
- In the CCLT.
- In Barclays Live.
- In the trade recap you send them after each transaction.


{qzsubmit}
==============================
Submit


{next}
==============================
Continue


{back}
==============================
Back