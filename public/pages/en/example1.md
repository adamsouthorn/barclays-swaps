{header}
==============================
## 2019 Swaps and Derivatives Regulation Mandatory Training Module

{b1}
==============================
Click


{rvl}
==============================
Do nulla Lorem ullamco aliquip adipisicing eleifend eiusmod laborum tempus labore qui cillum exercitation sit. Nisi consectetur eiusmod est quis cillum foo pariatur ad est consequat ad occaecat aliqua magna. Voluptate in labore dolor esse veniam deserunt sint tempus laboris tempor cillum excepteur nec aliqua.


{q1}
==============================
- Option A
- Option B
- Option C

{q1submit}
==============================
OK


{fbc}
==============================
Enim officia consectetur qui consectetur exercitation labore mollit excepteur leo nostrud eget ex est barfoo. Minim elit nostrud ex laborum adipisicing sit officia commodo consectetur magna eget sit dolor sit. Aliqua aute culpa quis nisi sint dolore cillum ipsum id enim est commodo tempus proident.

{fbi}
==============================
Leo parturient excepteur et parturient voluptate ad reprehenderit adipisicing quis voluptate deserunt velit eu elit. Non nostrud dolor elit cupidatat do in eget tempor Aenean magna dolore eget nulla sunt. Voluptate reprehenderit velit nisi labore ea elit consequat pariatur parturient eleifend adipisicing qui barfoo reprehenderit.

{next}
==============================
Continue