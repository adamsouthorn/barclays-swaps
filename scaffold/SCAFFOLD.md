# Cadence: Scaffolding

## Usage

Scaffolding is a fast way of creating a prototype of a course.

The `scaffold` folder contains a definition file in YAML format (`scaffold.yaml`) and a set of templates for pages and components.

The `scaffold.yaml` definition file is used to generate the `pages/en/*.vue` and `pages/en/*.md` files, along with the `router/structure.json` file.

To generate the files, run this command:

```bash
npm run gen
```

To override existing files use the `--force` flag.


### YAML style guide

```bash
010-010:         # page ID
  template:
    page-reveal	 # template file

  default:       # default slot
    intro:       # component ref
      t          # component type
    btn:
      b select   # component type with option

  reveals:       # custom slot
    rvl:
      t ? btn    # component type with condition
```

## Component Syntax

```
component[*N] [type] [flags] [? condition]
```

### Syntax examples

```bash
t ? q
# t - text component
# ? q - show if q component is done
 
 
b next
# b - button component
# next - set as a next button
 
 
mcq -p 100 -c a c
# mcq - multiple choice question component
# -p 100 - pass mark of 100
# -c a c - set options A and C as the correct answer
```

### Type

Optional. Use to define the type of component.

### Flags

Optional. Favour short options over long (e.g. `-p 100` not `--pass=100`).

### Condition

Optional. Conditionally show a component based on whether a component has been done (e.g. `t ? q`) or a score condition (e.g. `t ? q > 80`).


## Multiplication and auto-numbering

```bash
btn*3:
  b select
```
is equivalent to:

```bash
btn0:
  b select

btn1:
  b select

btn2:
  b select
```
and

```bash
rvl*3:
  t ? b$
```
is equivalent to:

```bash
rvl0:
  t ? b0

rvl1:
  t ? b1

rvl2:
  t ? b2
```

## Templates

The templates used for scaffolding are [EJS](https://www.ejs.co/) files. They are used to define the code chunks for components in the `.vue` files and the placeholder text for the `.md` files.

A basic example:

- t.vue.ejs:
  ```html
  <div v-html="md.<%= ref %>.html" />
  ```
- t.md.ejs:
  ```markdown
  {<%= ref %>}
  ==============================
  <%= ipsum.paragraph(3) %>
  ```
- scaffold.yaml:
  ```bash
  intro:
    t
  ```

generates:

- vue:
  ```html
  <div v-html="md.intro.html" />
  ```
- md:
  ```markdown
  {intro}
  ==============================
  Foo proident ipsum consectetur est amet enim sint commodo aliquip dolore culpa nostrud proident dolore. Dolore quis exercitation ex duis bar culpa aute sit esse nostrud dolor esse sit eget. Culpa eleifend est sit qui in esse commodo irure tempor eu exercitation excepteur nam Aenean.
  ```

### Template parameters

The templates receive a set of parameters that can be used to define the `.ejs` files:

For `*.vue.ejs`:

```javascript
{
  ref,    // the component ref
  args,   // the flags as defined in scaffold.yaml
  vif,    // the condition if defined in scaffold.yaml
  vfor,   // the number of interations if defined in scaffold.yaml
  parser  // the argument parser (https://github.com/yargs/yargs-parser)
}
```

For `*.md.ejs`:

```javascript
{
  ref,    // the component ref
  args,   // the flags as defined in scaffold.yaml
  ipsum,   // random lorem ipsum generator (https://github.com/neetjn/foobar-ipsum)
  parser  // the argument parser (https://github.com/yargs/yargs-parser)
}
```
