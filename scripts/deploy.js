const request = require('request')
const promptly = require('promptly')
const { basename, resolve } = require('path')
const { userInfo } = require('os')
const {
  createReadStream,
  unlinkSync,
  readFileSync,
  existsSync,
  readdirSync
} = require('fs')
const { getToken } = require('studio-auth/lib/auth')
const rimraf = require('rimraf')

const title = process.env.npm_package_cadence_title
const ver = process.env.npm_package_version || ''
const name = process.env.npm_package_name || ''
const lang = process.env.npm_config_lang || ''

const zip = `${kebabCase(title)}-${ver}.zip`
const zipUp = require('./zip-up')
const dest = resolve('dist', zip)


const userEnv = process.env.npm_config_user
const host = 'https://custom.lumesse.com/deploy'

function kebabCase(string) {
  return string
    .replace(/\s+/g, '-')
    .replace(/[^\w\-]+/g, '')
    .toLowerCase()
}

const titleCase = str => str.replace(/\w+/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase())

async function run() {
  let username = titleCase(userInfo().username)
  if (userEnv) {
    username = await promptly.prompt('Custom server username:')
  }

  let data = {
    title,
    client: process.env.npm_package_cadence_client,
    projectCode: name,
    language: lang,
    version: ver,
    uploadedBy: username
  }

  // Remove other langs
  if (lang) {
    var dir = readdirSync(`dist/${lang}/pages`)
    dir.forEach(f => {
      if (f !== lang) {
        rimraf(`dist/${lang}/pages/${f}`, (error) => {
          if (error) console.log(error)
        })
      }
    })
  }

  try {
    var zipResult = await zipUp('dist/' + (lang || 'all'), dest)
    // Send to custom server
    await deploy(dest, data)
  } catch (err) {
    console.error('Archive error:', err.message)
  }
  console.log(zipResult)
}

async function deploy(filepath, params = {}) {
  const token = await getToken()
  const fStream = createReadStream(filepath, { bufferSize: 64 * 1024 })

  // Remove file after upload
  fStream.on('end', (error) => {
    if (!error) {
      unlinkSync(filepath)
      console.log('Project transfered to server. Waiting for response....')
    }
  })

  const options = {
    method: 'POST',
    url: host,
    headers: {
      'cache-control': 'no-cache',
      'content-type': 'application/x-www-form-urlencoded, multipart/form-data',
      'user-agent': 'cadence-deploy',
      Authorization: `Bearer ${token}`,
      'x-studio-user': params.uploadedBy
    },
    formData: {
      payload: {
        value: fStream,
        options: {
          filename: basename(filepath),
          username: params.uploadedBy,
          contentType: null
        }
      }
    }
  }

  // Merge data
  options.formData = Object.assign(options.formData, params)

  // Make request
  request(options, (err, res, body) => {
    if (err) throw new Error(err)
    return console.log(body + '\nGenerate a public link from http://10.12.24.224:3001')
  })
}

run()
