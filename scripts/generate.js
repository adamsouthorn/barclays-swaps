const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml')
const parseArgs = require('yargs-parser')
const ejs = require('ejs')
const prettier = require('prettier')
const Fipsum = require('foobar-ipsum')
const ipsum = new Fipsum()

const lang = process.env.npm_config_lang || 'en'
const force = process.env.npm_config_force

const dirScaffold = './scaffold'
const scaffold = yaml.safeLoad(fs.readFileSync(`${dirScaffold}/scaffold.yaml`, 'utf8'))
const struct = { topics: {} }
const pathStruct = './src/router/structure.json'
const structJson = require(path.resolve(__dirname, '..', pathStruct))
const dirVue = `./src/pages`
const dirMd = `./public/pages/${lang}`
const templates = {}

async function generate() {
  for (const pageId in scaffold) {
    const pathVue = `${dirVue}/${pageId}.vue`

    // structure
    const topicId = pageId.split('-')[0]
    if (!struct.topics[topicId]) struct.topics[topicId] = []
    struct.topics[topicId].push(pageId)

    if (!force && fs.existsSync(pathVue)) continue

    const page = scaffold[pageId]
    let o = {
      id: pageId,
      default: ''
    }
    let md = ''

    // Render components and markdown

    for (const key in page) {
      if (key === 'template') continue

      // groups
      const groupName = key
      const group = page[groupName]

      if (!o[groupName]) o[groupName] = ''

      // components
      for (const comp in group) {
        const cmd = group[comp].split(' ? ')
        const args = parseArgs(cmd[0])
        const type = args._[0]
        const condition = cmd[1]
        let vif

        if (condition) {
          const numCompare = /(\w+)\s*(==|!=|<=?|>=?)\s*(\d*)/.test(condition)
          if (numCompare) {
            vif = 'scores.' + condition
          } else {
            const match = condition.match(/(\w+)(\$)?\W*/)
            const ref = match[1]
            const numbering = match[2]
            const res = deepSearch(page, ref)
            const data = res ? res.split(' ')[1] : null
            if (data === 'select') {
              vif = `selected === '${ref}'`
            } else if (numbering) {
              vif = ref
            } else {
              vif = 'done.' + ref
            }
          }
        }

        let compRef = comp

        const vforMatch = compRef.match(/(\w+)\*(\d)/)
        let vfor
        if (vforMatch) {
          compRef = vforMatch[1]
          vfor = +vforMatch[2]
        }

        if (!templates[type]) {
          templates[type] = {
            vue: fs.readFileSync(`${dirScaffold}/templates/${type}.vue.ejs`, 'utf8'),
            md: fs.readFileSync(`${dirScaffold}/templates/${type}.md.ejs`, 'utf8')
          }
        }

        o[groupName] += await ejs.render(templates[type].vue, { ref: compRef, args: cmd[0], vif, vfor, parser: parseArgs })

        // markdown
        for (let i = 0; i < (vfor || 1); i++) {
          const rendered = await ejs.render(templates[type].md, { ref: compRef + (vfor ? i : ''), args: cmd[0], ipsum, parser: parseArgs })
          md += rendered + '\n'
        }
      }
    }

    // Render and write page

    if (!templates[page.template]) {
      templates[page.template] = fs.readFileSync(`${dirScaffold}/templates/${page.template}.vue.ejs`, 'utf8')
    }
    const p = await ejs.render(templates[page.template], { page: o })

    if (!fs.existsSync(dirVue)) fs.mkdirSync(dirVue, { recursive: true })

    fs.writeFileSync(pathVue, prettier.format(p, { semi: false, singleQuote: true, parser: 'vue', htmlWhitespaceSensitivity: 'css' }))
    console.log(`${pathVue} created`)

    if (!fs.existsSync(dirMd)) fs.mkdirSync(dirMd, { recursive: true })

    const pathMd = `${dirMd}/${pageId}.md`
    fs.writeFile(pathMd, md, (err) => {
      if (err) throw err
      console.log(`${pathMd} created`)
    })
  }

  // Write structure.json

  const sortedTopics = Object.keys(struct.topics).sort()
  let json = { topics: [] }

  sortedTopics.forEach(t => {
    const sortedPages = struct.topics[t].sort()
    json.topics.push({ id: t, pages: [] })
    sortedPages.forEach(p => {
      json.topics[json.topics.length - 1].pages.push({ id: p })
    })
  })

  fs.writeFileSync(pathStruct, JSON.stringify(json, null, 2))
  console.log(`${pathStruct} created`)

  // Write menu.md

  const pathMenu = `${dirMd}/menu.md`
  const origMenu = { topics: [] }
  const menuJson = { topics: [] }

  if (fs.existsSync(pathMenu)) {
    const lines = fs.readFileSync(pathMenu, 'utf8').split('\n')
    const orig = { topics: [] }

    lines.forEach(l => {
      const matchTopic = l.match(/^- (.*)/)
      if (matchTopic) {
        orig.topics.push({ name: matchTopic[1], pages: [] })
      } else {
        const matchPage = l.match(/^\s+- (.*)/)
        if (matchPage) {
          orig.topics[orig.topics.length - 1].pages.push({ name: matchPage[1] })
        }
      }
    })

    structJson.topics.forEach((t, i) => {
      origMenu.topics.push({
        id: t.id,
        name: orig.topics[i] ? orig.topics[i].name : 'Topic ' + t.id,
        pages: []
      })
      t.pages.forEach((p, j) => {
        origMenu.topics[i].pages.push({
          id: p.id,
          name: orig.topics[i] && orig.topics[i].pages[j] ? orig.topics[i].pages[j].name : 'Page ' + p.id
        })
      })
    })
  }

  let nT = 0

  json.topics.forEach((t, i) => {
    let nP = 0
    const origT = origMenu.topics[nT]

    if (origT && origT.id === t.id) {
      menuJson.topics.push({
        id: t.id,
        name: origT.name,
        pages: []
      })
      nT++
    } else {
      menuJson.topics.push({
        id: t.id,
        name: 'Topic ' + t.id,
        pages: []
      })
    }

    t.pages.forEach((p, j) => {
      let origP = null
      if (origT) {
        origP = origT.pages[nP]
      } else {
        origP = origMenu.topics[i] ? origMenu.topics[i].pages[nP] : null
      }

      if (origP && origP.id === p.id) {
        menuJson.topics[menuJson.topics.length - 1].pages.push({
          id: p.id,
          name: origP.name
        })
        nP++
      } else {
        menuJson.topics[menuJson.topics.length - 1].pages.push({
          id: p.id,
          name: 'Page ' + p.id
        })
      }
    })
  })

  const renderedMenu = await ejs.render(fs.readFileSync(`${dirScaffold}/templates/menu.md.ejs`, 'utf8'), { struct: menuJson })

  fs.writeFileSync(pathMenu, renderedMenu)
  console.log(`${pathMenu} created`)
}


function deepSearch(o, key) {
  for (const p in o) {
    if (p === key) {
      return o[p]
    }
    if (typeof o[p] === 'object') {
      for (const q in o[p]) {
        if (q === key) {
          return o[p][q]
        }
      }
    }
  }
}


generate()
