const fs = require('fs')
const archiver = require('archiver')

module.exports = (dir, zip) => {
  return new Promise((resolve, reject) => {
    const output = fs.createWriteStream(zip)

    const archive = archiver('zip', {
      zlib: { level: 9 } // compression level
    })

    output.on('close', () => {
      resolve(`Zip created: ${zip} (${archive.pointer()} bytes)`)
    })

    archive.on('error', err => {
      reject(err)
    })

    archive.pipe(output)

    archive.glob('**', {
      cwd: dir
    })

    archive.finalize()
  })
}
