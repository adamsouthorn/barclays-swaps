import statuses from '@/utils/StatusTypes.js'
import successes from '@/utils/SuccessTypes.js'

export default {
  props: {
    persist: {
      type: String,
      default: null
      // valid: completion|success|''
    }
  },

  computed: {
    ref() {
      return this.$vnode.data.ref
    },

    pageId() {
      return this.$store.state.pageId
    },

    storeData() {
      if (!this.$store.state.interactions[this.pageId]) return {}
      return this.$store.state.interactions[this.pageId][this.ref] || {}
    },

    completion: {
      get() {
        if (!this.$store) return statuses.INCOMPLETE
        return this.storeData.completion || null
      },
      set(val) {
        if (!this.$store) return
        this.$store.dispatch('interactions/setCompletion', { page: this.pageId, ref: this.ref, val })
      }
    },

    success: {
      get() {
        if (!this.$store) return successes.UNKNOWN
        return this.storeData.success || null
      },
      set(val) {
        if (!this.$store) return
        this.$store.dispatch('interactions/setSuccess', { page: this.pageId, ref: this.ref, val })
      }
    }
  },

  created() {
    // Reset statuses if not persisting
    if (!this.completion || !this.willPersist('completion')) this.completion = statuses.INCOMPLETE
    if (!this.willPersist('success')) this.success = successes.UNKNOWN
  },

  methods: {
    willPersist(val) {
      if (this.persist === null) return false
      return this.persist.includes(val) || this.persist === ''
    }
  }
}
