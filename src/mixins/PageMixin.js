import statuses from '@/utils/StatusTypes'
import loadMd from '@/services/LoadMarkdown'

export default {
  props: {
    markdown: {
      type: Object,
      default: null
    }
  },

  data() {
    return {
      md: this.markdown,
      pageId: '',
      selected: ''
    }
  },

  computed: {
    done() {
      const o = {}
      const completions = this.$store.state.interactions[this.pageId]
      if (completions) {
        Object.keys(completions).map(ref => {
          o[ref] = completions[ref].completion === statuses.COMPLETE
        })
      }
      return o
    },

    scores() {
      const o = {}
      const completions = this.$store.state.interactions[this.pageId]
      if (completions) {
        Object.keys(completions).map(ref => {
          const score = completions[ref].score
          if (typeof score !== 'undefined' && score !== null) o[ref] = score
        })
      }
      return o
    },

    pageCompletion: {
      get() {
        return this.$store.getters['progress/completion'](this.pageId)
      },
      set(val) {
        this.$store.dispatch('progress/setCompletion', { page: this.pageId, status: val })
      }
    }
  },

  beforeRouteUpdate(to, from, next) {
    // catch lang changes
    const lang = to.query.lang || 'en'
    loadMd(lang, this.pageId).then(res => {
      this.md = res
      next()
    })
  },

  created() {
    if (this.pageCompletion === 'unknown') this.pageCompletion = statuses.COMPLETE
  }
}
