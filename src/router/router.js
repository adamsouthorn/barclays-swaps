import Vue from 'vue'
import Router from 'vue-router'
import struct from './structure.json'
import loadMd from '@/services/LoadMarkdown'
import store from '@/store/store'
/* importing store is needed for the beforeRouteEnter event */

Vue.use(Router)

const routes = [{ path: '*', redirect: '/0/0' }]
const topicIds = Object.keys(struct.topics)

topicIds.forEach((topicId, i) => {
  const topic = struct.topics[topicId]
  topic.pages.forEach((page, j) => {
    routes.push({
      path: '/' + i + '/' + j,
      name: page.id,
      component: () => import('@/pages/' + page.id + '.vue'),
      props: true,
      // ensure markdown is loaded before the page
      beforeEnter(to, from, next) {
        const lang = store.state.lang
        loadMd(lang, page.id).then(res => {
          to.params.markdown = res
          next()
        })
      }
    })
    // use page id as an alternative path
    routes.push({
      path: '/' + page.id,
      redirect: '/' + i + '/' + j
    })
  })
})

export default new Router({
  routes,
  scrollBehavior() { return { y: 0 } }
})
