import axios from 'axios'
import cadMd from '@/utils/CadenceMarkdown.js'

export default (lang, fileId) => {
  return new Promise((resolve, reject) => {
    axios.get(`./pages/${lang}/${fileId}.md`)
      .then(res => {
        resolve(cadMd(res.data))
      })
      .catch(err => {
        reject(err)
      })
  })
}
