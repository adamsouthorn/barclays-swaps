import Vue from 'vue'

export default {
  namespaced: true,

  state: {},

  mutations: {
    set(state, { page, ref, key, val }) {
      if (!state[page]) Vue.set(state, page, {})
      if (!state[page][ref]) Vue.set(state[page], ref, {})
      Vue.set(state[page][ref], key, val)
    },

    reset(state) {
      Object.keys(state).forEach(key => {
        delete state[key]
      })
    }
  },

  actions: {
    setCompletion({ commit }, payload) {
      commit('set', { ...payload, key: 'completion' })
    },

    setSuccess({ commit }, payload) {
      commit('set', { ...payload, key: 'success' })
    },

    setScore({ commit }, payload) {
      commit('set', { ...payload, key: 'score' })
    },

    setInput({ commit }, payload) {
      commit('set', { ...payload, key: 'input' })
    }
  }
}
