import struct from '@/router/structure.json'
import statuses from '@/utils/StatusTypes'
import successes from '@/utils/SuccessTypes'

function createProgressFromStructure(struct) {
  var o = {}
  struct.topics.forEach(topic => {
    o[topic.id] = {
      completion: statuses.UNKNOWN
    }
    topic.pages.forEach(page => {
      o[topic.id][page.id] = statuses.UNKNOWN
    })
  })
  return o
}

function isOptional(ref) {
  const id = ref.split('-')
  const topicId = id[0]
  const topic = struct.topics.filter(topic => topic.id === topicId)[0]
  let optional = false

  if (id.length > 1) {
    optional = topic.pages.filter(page => page.id === ref)[0].optional || false
  } else {
    optional = topic.optional || false
  }

  return optional
}

export default {
  namespaced: true,

  state: {
    completion: statuses.INCOMPLETE,
    success: successes.UNKNOWN,
    ...createProgressFromStructure(struct)
  },

  getters: {
    completion: (state) => (id) => {
      const ref = id.split('-')
      return ref.length === 2 ? state[ref[0]][id] : state[ref[0]].completion
    }
  },

  mutations: {
    setOverallCompletion(state, status) {
      state.completion = status
    },
    setOverallSuccess(state, status) {
      state.success = status
    },
    setTopicCompletion(state, { topic, status }) {
      state[topic].completion = status
    },
    setPageCompletion(state, { page, status }) {
      const topicId = page.split('-')[0]
      state[topicId][page] = status
    },
    reset(state) {
      Object.keys(state).filter(key => key.match(/\d{3}/)).forEach(topicId => {
        delete state[topicId]
      })
      state.completion = statuses.INCOMPLETE
      state.success = successes.UNKNOWN
      const newStruct = createProgressFromStructure(struct)
      for (const topicId in newStruct) {
        state[topicId] = newStruct[topicId]
      }
    },
    replace(state, newStructure) {
      Object.keys(state).filter(key => key.match(/\d{3}/)).forEach(topicId => {
        delete state[topicId]
      })
      for (const topicId in newStructure) {
        state[topicId] = newStructure[topicId]
      }
    }
  },

  actions: {
    setCompletion({ state, commit, dispatch }, { page, status }) {
      if (typeof page === 'undefined') {
        commit('setOverallCompletion', status)
        return
      }

      const id = page.split('-')
      const topicId = id[0]

      if (id.length > 1) {
        commit('setPageCompletion', { page, status })

        // complete topic if all pages are complete
        const topic = state[topicId]
        const pageIds = Object.keys(topic).filter(pageId => /\d{3}-\d{3}/.test(pageId) && !isOptional(pageId))
        const numComplete = pageIds.reduce((acc, pageId) => {
          return topic[pageId] === statuses.COMPLETE ? acc + 1 : acc
        }, 0)
        if (numComplete === pageIds.length) {
          dispatch('setCompletion', { page: topicId, status: statuses.COMPLETE })
        } else {
          dispatch('setCompletion', { page: topicId, status: statuses.INCOMPLETE })
        }
      } else {
        commit('setTopicCompletion', { topic: topicId, status })

        // complete overall if all topics are complete
        const topicIds = Object.keys(state).filter(topicId => /\d{3}/.test(topicId) && !isOptional(topicId))
        const numComplete = topicIds.reduce((acc, topicId) => {
          return state[topicId].completion === statuses.COMPLETE ? acc + 1 : acc
        }, 0)
        if (numComplete === topicIds.length) {
          commit('setOverallCompletion', statuses.COMPLETE)
        }
      }
    },

    refresh({ state, commit }) {
      const newStruct = createProgressFromStructure(struct)
      const topicIds = Object.keys(newStruct).filter(key => key.match(/\d{3}/))

      topicIds.forEach(topicId => {
        const topic = newStruct[topicId]
        topic.completion = state[topicId].completion
        const pageIds = Object.keys(topic).filter(key => key.match(/\d{3}-\d{3}/))
        pageIds.forEach(pageId => {
          topic[pageId] = state[topicId][pageId]
        })
      })

      commit('replace', newStruct)
    }
  }
}
