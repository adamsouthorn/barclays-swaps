import Vue from 'vue'
import Vuex from 'vuex'
import progress from './modules/progress'
import interactions from './modules/interactions'
import persistPlugin from 'vuex-persistedstate'
import camelCase from 'lodash/camelCase'
import merge from 'lodash/merge'
import { SCORM, debug as scormDebug } from 'pipwerks-scorm-api-wrapper'
import scormPlugin, { msToCmiTime } from './vuex-scorm'

Vue.use(Vuex)

var persistentKeys = [
  'bookmark',
  'lang',
  'score',
  'progress',
  'interactions'
]


var plugins = []

scormDebug.isActive = false
SCORM.version = '1.2'
var usingSCORM = SCORM.init()

if (usingSCORM) {
  plugins.push(scormPlugin(persistentKeys))

  var startTime = new Date().getTime()
  window.addEventListener('unload', () => {
    if (SCORM.version === '1.2') {
      SCORM.set('cmi.core.session_time', msToCmiTime(new Date().getTime() - startTime))
    }
    SCORM.quit()
  })
} else {
  plugins.push(persistPlugin({
    key: camelCase(document.title + document.body.getAttribute('data-ver')),
    paths: persistentKeys
  }))
}

var initialState = {
  bookmark: '',
  pageId: '',
  score: null,
  scorm: usingSCORM ? SCORM.version : 'no',
  lang: document.querySelector('html').getAttribute('lang'),
  debug: false
}

export default new Vuex.Store({
  state: merge({}, initialState),

  mutations: {
    setBookmark(state, path) {
      state.bookmark = path
    },
    setPageId(state, id) {
      state.pageId = id
    },
    setLang(state, lang) {
      state.lang = lang
    },
    setOverallScore(state, score) {
      state.score = score
    },
    setDebug(state, val) {
      state.debug = val
    },
    reset(state) {
      Object.keys(initialState).forEach(key => {
        if (key !== 'debug') state[key] = initialState[key]
      })
    }
  },

  actions: {
    reset({ commit }) {
      commit('reset')
      commit('progress/reset')
      commit('interactions/reset')
    }
  },

  modules: {
    progress,
    interactions
  },

  plugins
})
