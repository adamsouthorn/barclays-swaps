import { SCORM } from 'pipwerks-scorm-api-wrapper'
import merge from 'lodash/merge'
import invert from 'lodash/invert'
import statuses from '@/utils/StatusTypes.js'
import successes from '@/utils/SuccessTypes.js'


function getState() {
  var data = SCORM.get('cmi.suspend_data')
  try {
    return data && data !== 'undefined' ? JSON.parse(decompress(data)) : undefined
  } catch (err) {
    return undefined
  }
}


function setState(state) {
  SCORM.set('cmi.suspend_data', compress(JSON.stringify(state)))
  try {
    SCORM.save()
  } catch (err) {
    console.log('Error saving to LMS: ' + err)
  }
}


const lookup = {
  'completion': 'c',
  [statuses.COMPLETE]: 'd',
  [statuses.INCOMPLETE]: 'i',
  'success': 's',
  [successes.PASS]: 'p',
  [successes.FAIL]: 'f',
  'unknown': 'u'
}

const reverseLookup = invert(lookup)

function compress(str, lookupTable = lookup) {
  let s = str
  for (let key in lookupTable) {
    s = s.replace(new RegExp('"' + key + '"', 'g'), '"' + lookupTable[key] + '"')
  }
  return s
}

function decompress(str) {
  return compress(str, reverseLookup)
}


export default function(validKeys = []) {
  return store => {
    const v = SCORM.version

    // save data after every vuex mutation
    store.subscribe((mutation, state) => {
      if (mutation.type === 'progress/setOverallCompletion') {
        let status = statuses.INCOMPLETE
        if (mutation.payload === statuses.COMPLETE) status = 'completed'
        SCORM.set(`cmi.${v === '2004' ? 'completion' : 'core.lesson'}_status`, status)
      }

      if (mutation.type === 'progress/setOverallSuccess' && v === '2004') {
        let status
        switch (mutation.payload) {
          case successes.PASS:
            status = 'passed'
            break
          case successes.FAIL:
            status = 'failed'
            break
          default:
            status = 'unknown'
        }
        SCORM.set('cmi.success_status', status)
      }

      if (mutation.type === 'setOverallScore') {
        SCORM.set(`cmi.${v === '2004' ? '' : 'core.'}score.min`, 0)
        SCORM.set(`cmi.${v === '2004' ? '' : 'core.'}score.max`, 100)
        SCORM.set(`cmi.${v === '2004' ? '' : 'core.'}score.raw`, mutation.payload)
      }

      if (validKeys.length === 0) {
        setState(state)
      } else {
        const filteredState = Object.keys(state).filter(k => validKeys.includes(k)).reduce((o, k) => {
          o[k] = state[k]
          return o
        }, {})
        setState(filteredState)
      }
    })

    // get data from LMS
    var lmsState = getState()
    if (typeof lmsState === 'object') {
      store.replaceState(merge({}, store.state, lmsState))
    } else {
      // first visit or has been reset
      store.commit('reset')
    }
  }
}

export function msToCmiTime(ms, excludeFraction = false) {
  var ts = ms / 1000
  var sec = (ts % 60)

  ts -= sec
  var tmp = (ts % 3600) // # of seconds in the total # of minutes
  ts -= tmp // # of seconds in the total # of hours

  // convert seconds to conform to CMITimespan type (e.g. SS.00)
  sec = Math.round(sec * 100) / 100

  var strSec = sec.toString()
  var strWholeSec = strSec
  var strFractionSec = ''

  if (strSec.indexOf('.') !== -1) {
    strWholeSec = strSec.substring(0, strSec.indexOf('.'))
    strFractionSec = strSec.substring(strSec.indexOf('.') + 1, strSec.length)
  }

  if (strWholeSec.length < 2) {
    strWholeSec = '0' + strWholeSec
  }
  strSec = strWholeSec

  if (strFractionSec.length && !excludeFraction) {
    strSec = strSec + '.' + strFractionSec
  }

  var hour, min
  if ((ts % 3600) !== 0) { hour = 0 } else hour = (ts / 3600)
  if ((tmp % 60) !== 0) { min = 0 } else min = (tmp / 60)

  if (hour.toString().length < 2) { hour = '0' + hour }
  if (min.toString().length < 2) { min = '0' + min }

  var rtnVal = hour + ':' + min + ':' + strSec

  return rtnVal
}
