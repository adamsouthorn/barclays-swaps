/**
 * Transforms a Markdown string
 * to a Cadence page object.
 */

import mdIt from 'markdown-it'
import mdSections from 'markdown-it-sections-by-headers'
import mdInline from 'markdown-it-for-inline'

const mdParser = mdIt()
  .use(mdSections, { maxLevel: 1 })
  .use(mdInline,
    'url_new_win',
    'link_open',
    (tokens, idx) => {
      let aIndex = tokens[idx].attrIndex('target')
      if (aIndex < 0) {
        tokens[idx].attrPush(['target', '_blank'])
      } else {
        tokens[idx].attrs[aIndex][1] = '_blank'
      }
    })

export default (md) => {
  const data = {}
  // Parse MD to HTML
  const source = mdParser.render(md)

  // Use browser's DOM
  const doc = document.implementation.createHTMLDocument(null)
  doc.documentElement.innerHTML = source

  // Add each section to data object
  const sections = [...doc.querySelectorAll('section')]
  sections.forEach(sec => {
    const h1 = sec.querySelector('h1')
    const ref = h1.textContent.replace(/{|}/g, '')
    sec.removeChild(h1)
    data[ref] = {}
    const lis = [...sec.querySelectorAll('li')]
    if (lis.length > 0) {
      data[ref].raw = []
      lis.forEach(li => {
        data[ref].raw.push(li.textContent)
      })
    } else {
      data[ref].raw = sec.textContent.trim()
    }
    data[ref].html = sec.innerHTML.trim()
  })

  return data
}
