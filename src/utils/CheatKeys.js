/**
 * Defines a key phrase and fires an event when
 * the phrase is entered via the keyboard.
 *
 * @author Joe Chung
 */

export default function(phrase, win, callback) {
  var _cheat = phrase.toUpperCase()
  var _callback = callback
  var _keysTyped = []
  win.addEventListener('keydown', addKey)

  function addKey(event) {
    _keysTyped.push(numToChar(event.keyCode))
    if (validate()) _callback()
  }

  function validate() {
    if (_keysTyped.join('') === _cheat) {
      _keysTyped = []
      return true
    }
    // Clear the array if the key does not match
    // the relevant character in the cheat string
    for (var i = 0; i < _keysTyped.length; i++) {
      if (_keysTyped[i] !== _cheat.substr(i, 1)) {
        _keysTyped = []
        return false
      }
    }
  }

  function numToChar(num) {
    var str
    if (num > 64 && num < 91) {
      str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      return str.charAt(num - 65)
    } else if (num > 96 && num < 123) {
      str = 'abcdefghijklmnopqrstuvwxyz'
      return str.charAt(num - 97)
    } else {
      return ''
    }
  }
}
