/**
 *  Checks the selected options against the answer.
 *
 *  @param  selected          Array (of type string) of the selected options.
 *  @param  includeIncorrect  Boolean to include incorrect options in the calculations.
 *  @return                   The evaluated score as a percentage between 0 and 100.
 *  @author Joe Chung
 */
export default function(choices, solution, selected, includeIncorrect = true) {
  const len = choices.length
  const solLen = solution.length
  const selLen = selected.length

  // If solution is empty then any selection is correct
  if (solLen === 0) return 100
  if (selLen === 0) return 0

  const count = choices.reduce((score, opt, i, a) => {
    if (solution.includes(opt) && selected.includes(opt)) {
      score++
    }
    if (includeIncorrect && (!solution.includes(opt) && !selected.includes(opt))) {
      score++
    }
    return score
  }, 0)

  const total = includeIncorrect ? len : solLen
  return Math.round(count / total * 100)
}
