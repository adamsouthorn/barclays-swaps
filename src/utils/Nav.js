import router from '@/router/router'
import struct from '@/router/structure.json'

function getNextPage() {
  const [topic, page] = router.currentRoute.path.split('/').slice(1).map(Number)

  if (page === struct.topics[topic].pages.length - 1) {
    return `/${topic + 1}/0`
  }

  return `/${topic}/${page + 1}`
}

function getPrevPage() {
  const [topic, page] = router.currentRoute.path.split('/').slice(1).map(Number)

  if (page === 0 && topic === 0) return ''

  if (page === 0) {
    return `/${topic - 1}/${struct.topics[topic - 1].pages.length - 1}`
  }

  return `/${topic}/${page - 1}`
}

function gotoAdjacentPage(dir) {
  const location = { path: dir === 'prev' ? getPrevPage() : getNextPage() }
  const queryLang = router.currentRoute.query.lang
  if (queryLang) location.query = { lang: queryLang }
  router.push(location)
}

function gotoNextPage() {
  gotoAdjacentPage('next')
}

function gotoPrevPage() {
  gotoAdjacentPage('prev')
}

function getBeginning() {
  router.push('/0/0')
}

function getExit() {
  self.close()
}

export { getNextPage, getPrevPage, gotoAdjacentPage, gotoNextPage, gotoPrevPage, getBeginning, getExit }
