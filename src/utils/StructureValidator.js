import store from '@/store/store'
import struct from '@/router/structure.json'

export default {
  validate() {
    const progress = store.state.progress
    const topicIds = Object.keys(progress).filter(key => key.match(/\d{3}/))
    const topicsEqual = topicIds.length === struct.topics.length

    if (!topicsEqual) return false

    for (let i = 0; i < topicIds.length; i++) {
      const topicId = topicIds[i]
      const pageIds = Object.keys(progress[topicId]).filter(key => key.match(/\d{3}-\d{3}/))
      if (pageIds.length !== struct.topics[i].pages.length) return false
    }

    return true
  }
}
