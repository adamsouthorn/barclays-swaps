export default {
  PASS: 'pass',
  FAIL: 'fail',
  UNKNOWN: 'unknown'
}
