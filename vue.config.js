// For all available config options see:
// https://github.com/vuejs/vue-cli/blob/dev/docs/config.md

const ImageminPlugin = require('imagemin-webpack-plugin').default

let config = {
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',

  transpileDependencies: [
    /cadence-\w+/,
    'scroll-indicator',
    'smooth-subs'
  ]
}

if (process.env.NODE_ENV === 'development') {
  Object.assign(config, {
    css: {
      sourceMap: true
    },

    // set source maps to work in chrome + firefox
    configureWebpack: cfg => {
      cfg.devtool = 'eval-source-map'

      cfg.output.devtoolModuleFilenameTemplate = info => info.resourcePath.match(/^\.\/\S*?\.vue$/)
        ? `webpack-generated:///${info.resourcePath}?${info.hash}`
        : `webpack-cadence:///${info.resourcePath}`

      cfg.output.devtoolFallbackModuleFilenameTemplate = 'webpack:///[resource-path]?[hash]'
    }
  })
} else if (process.env.NODE_ENV === 'production') {
  Object.assign(config, {
    // where to output built files
    outputDir: 'dist/' + (process.env.npm_config_lang || 'all'),

    productionSourceMap: false,

    css: {
      sourceMap: false
    },
    configureWebpack: {
      plugins: [
        new ImageminPlugin({
          minFileSize: 100000,
          svgo: null
        }),
        new ImageminPlugin({
          test: 'img/*.svg',
          svgo: {
            plugins: [
              { removeViewBox: false }
            ]
          }
        })
      ]
    }
  })
}

module.exports = config
